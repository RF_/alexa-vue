import Vue from 'vue'
import App from './App.vue'
import router from './router'
import locale from 'element-ui/lib/locale/lang/fa'
import Element from 'element-ui'
import './element-variables.scss'
import './assets/style.scss'
import "@/plugins/echarts";
import store from './store.js';
import VueCurrencyFilter from 'vue-currency-filter';
import VueClipboard from 'vue-clipboard2'

Vue.config.productionTip = false
Vue.use(VueCurrencyFilter, {
  symbol : '',
  thousandsSeparator: ',',
  fractionCount: 0,
  fractionSeparator: '.',
  symbolPosition: 'front',
  symbolSpacing: true
})
Vue.use(VueCurrencyFilter)
Vue.use(Element, {locale})
Vue.use(VueClipboard)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
