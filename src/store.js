import createPersistedState from 'vuex-persistedstate';
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
      searched: []
    },
    mutations: {
      search (state, site) {
      let result = site.replace(/(^\w+:|^)\/\//, '');
      // let result = (new URL(site)).hostname;
      let i = state.searched.indexOf(result);
      if (i !== -1) state.searched.splice(i, 1);
      state.searched.unshift(result);
      const l = state.searched.length;
      if (l>6) {
        state.searched.splice(l-1,1)
      }
      },
    },
    getters: {
		sites: (state) => {
			return state.searched;
		},
    },
    actions: {
        adminAuth (state) {
            axios
            .post(
                `${process.env.VUE_APP_baseURL}/admin/auth`,
                {token: state.state.QAZAFUN_TOKEN},
                {
                  auth: {
                    username: process.env.VUE_APP_USERNAME,
                    password: process.env.VUE_APP_PASSWORD
                  }
                }
            ).then ((res) => {
              if (res.data.err) {
                this.commit("setAdmin", null);
                this.commit("setAdminToken", null);
                  return false;
              } else if(res.data.done) {
                this.commit("setAdmin", res.data.user);
                  return true;
              }
            }).catch (() => {
                this.state.qazafun_admin = null;
                this.state.QAZAFUN_TOKEN = null;
                return false;              
            })      
        },
        admin_exit() {
          this.state.qazafun_admin = null;
          this.state.QAZAFUN_TOKEN = null;
        }
    },
    plugins: [createPersistedState()],
  });

export default store;